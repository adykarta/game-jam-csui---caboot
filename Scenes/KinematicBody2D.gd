extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const UP = Vector2(0,-1)
var motion = Vector2()


			
# Called when the node enters the scene tree for the first time.
var jump = false
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if jump == false:
		$Sprite.play("Run")
	elif jump== true :
		$Sprite.play("Jump")
func _physics_process(delta):
	
	
	global.score +=1
	
	motion.y += delta * 1200

	if is_on_floor() and Input.is_action_just_pressed("ui_up"):
		jump = true
		var musicNode = $"Audio Effect/AudioStreamPlayer2D"
		musicNode.play()
		motion.y = -400
	
	elif is_on_floor():
		jump = false
	else:
		motion.x=0

	move_and_slide(motion, UP)
#	pass
