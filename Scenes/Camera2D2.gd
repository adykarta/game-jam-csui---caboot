extends Camera2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var offsett = 0
var speed = 270
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	offsett += speed * delta
	set_offset(Vector2(offsett,0))
	set_limit(1,-1)
#	pass
